// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "wall.generated.h"
//#include "Interactable.h"

UCLASS()
class SNAKEGAME_API Awall : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	Awall();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//virtual void Interact(AActor* Interactor, bool bIsHead) override;

private:
	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* WallMeshComponent;
};
