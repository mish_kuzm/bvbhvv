// Fill out your copyright notice in the Description page of Project Settings.


#include "TFood.h"
#include "SnakeBase.h"
#include "Components/StaticMeshComponent.h"

// Sets default values
ATFood::ATFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	FoodZCoordinate = 0;

	GenerateRandomLocation();
}

// Called when the game starts or when spawned
void ATFood::BeginPlay()
{
	Super::BeginPlay();

	UpdateFoodLocation();

	GetWorldTimerManager().SetTimer(FoodUpdateTimer, this, &ATFood::UpdateFoodLocation, 5.0f, true);
}

// Called every frame
void ATFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			UpdateFoodLocation();
		}
	}
}

void ATFood::UpdateFoodLocation()
{
	GenerateRandomLocation();

	SetActorLocation(FVector(NewFoodX, NewFoodY, FoodZCoordinate));
}

void ATFood::GenerateRandomLocation()
{
	NewFoodX = FMath::RandRange(-3000, 3000);
	NewFoodY = FMath::RandRange(-4000, 4000);
}