// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "TFood.generated.h"

UCLASS()
class SNAKEGAME_API ATFood : public AActor, public IInteractable
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATFood();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FTimerHandle FoodUpdateTimer;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	virtual void Interact(AActor* Interactor, bool bIsHead) override;

	UPROPERTY(EditAnywhere, Category = "Game Area")
	float MinX;

	UPROPERTY(EditAnywhere, Category = "Game Area")
	float MaxX;

	UPROPERTY(EditAnywhere, Category = "Game Area")
	float MinY;

	UPROPERTY(EditAnywhere, Category = "Game Area")
	float MaxY;

	// Function to update the food's location
	void UpdateFoodLocation();

private:
	// Generate random coordinates for the food
	void GenerateRandomLocation();

	float FoodZCoordinate;
	float NewFoodX;
	float NewFoodY;
};
